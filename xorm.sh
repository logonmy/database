#!/bin/bash

dbuser=root
dbpwd=""
goSrc=$GOPATH/src
xormSrc="github.com/go-xorm/xorm"
xormCmd="github.com/go-xorm/cmd"
nowPath=`pwd`
echo $nowPath
amountKeys="Corpus|Price|Amount|Balance|Earnings|Interests"

if [[ $# -lt 1 ]]; then
    echo "==> print: this is xorm models tool."
    echo "           If you get error messages about xorm or xorm's cmd,"
    echo "           you can add the flag: -x yes, then try again."
    echo "==> params are:"
    echo "    -s schema (you must input database schema name, no default)"
    echo "    -t schema (the go file will be put into the path, default: ./schema)"
    echo "    -g gopath (set go path, default: \$GOPATH)"
    echo "    -x yes (yes for update $xormSrc, others do not update)"
    echo "    -d yes (yes for delete old models, others do not delete)"
    echo "    -h localhost (connect database host, default: localhost)"
    echo "    -P 3306 (connect database port, default: 3306)"
    echo "    -u user (connect database user name, default: root)"
    echo "    -p password (connect database user password, default: '')"
    exit 0
fi

schema=""
dbpwd=''
xormUpdateFlag="no"
deleteOldModels="no"
host="localhost"
port="3306"
schemaPath=""
gopath=$GOPATH

function checkParam()
{
    if [[ ${schema:0:1} = "-" ]]; then
        echo "==> print: invalid parameter of $schema"
        exit 1
    fi
    if [[ ${xormUpdateFlag:0:1} = "-" ]]; then
        echo "==> print: invalid parameter of $xormUpdateFlag"
        exit 1
    fi
    if [[ ${dbuser:0:1} = "-" ]]; then
        echo "==> print: invalid parameter of $dbuser"
        exit 1
    fi
    if [[ ${deleteOldModels:0:1} = "-" ]]; then
        echo "==> print: invalid parameter of $deleteOldModels"
        exit 1
    fi
    if [[ ${host:0:1} = "-" ]]; then
        echo "==> print: invalid parameter of $host"
        exit 1
    fi
    if [[ ${port:0:1} = "-" ]]; then
        echo "==> print: invalid parameter of $port"
        exit 1
    fi
    if [[ ${schemaPath:0:1} = "-" ]]; then
        echo "==> print: invalid parameter of $schemaPath"
        exit 1
    fi
    if [[ $gopath = "" ]]; then
        gopath=$GOPATH
        if [[ ! -d $gopath ]]; then
            echo "==> print: invalid parameter of $schemaPath"
            exit 1
        fi
        export GOPATH=$gopath
        export PATH=$PATH:$gopath/bin
    fi
}

while getopts :t:s:x:u:p:d:h:P:g: otp
do
    case $otp in
        t)
            schemaPath=$OPTARG
            checkParam schemaPath
            ;;
        s)
            schema=$OPTARG
            checkParam schema
            ;;
        x)
            xormUpdateFlag=$OPTARG
            checkParam xormUpdateFlag
            ;;
        u)
            dbuser=$OPTARG
            checkParam dbuser
            ;;
        p)
            dbpwd=$OPTARG
            ;;
        d)
            deleteOldModels=$OPTARG
            checkParam deleteOldModels
            ;;
        h)
            host=$OPTARG
            checkParam host
            ;;
        P)
            port=$OPTARG
            checkParam port
            ;;
        g)
            gopath=$OPTARG
            checkParam gopath
            export GOPATH=$gopath
            export PATH=$PATH:$gopath/bin
            ;;
    esac
done

if [[ ! -d $gopath ]]; then
    echo "==> print: invalid parameter of $gopath"
    exit 1
fi
goSrc=$gopath/src

if [[ "$schema" = "" ]]; then
    echo "==> print: you must input database schema name."
    exit 1
fi

if [[ "$schemaPath" = "" ]]; then
    schemaPath="./$schema"
fi

dbConnector=""
if [[ "$dbpwd" = '' ]]; then
    dbConnector="$dbuser@tcp($host:$port)"
else
    dbConnector="$dbuser:$dbpwd@tcp($host:$port)"
fi
echo "==> print: $dbConnector."
echo "==> print: you want get $schema's models."

echo "==> print: start checking xorm's path, $xormSrc."
if [[ ! -d "$goSrc/$xormSrc" ]]; then
    echo "==> print: xorm path not in gopath"
    echo "==> print: $goSrc/$xormSrc"
    echo "==> print: go get $xormSrc"
    go get $xormSrc
    if [[ "$xormUpdateFlag" = "yes" ]]; then
        echo "==> print: xorm is new, no need update"
        xormUpdateFlag=noupdate
    fi
fi

if [[ "$xormUpdateFlag" = "yes" ]]; then
    echo "==> print: updating $xormSrc."
    go get -u $xormSrc
fi

echo "==> print: start go to build and install xorm."
cd $goSrc/$xormSrc
go build
go install

echo "==> print: start checking xorm cmd's path, $xormCmd."
if [[ ! -d "$goSrc/$xormCmd" ]]; then
    echo "==> print: xorm cmd's path is not exist."
    echo "==> print: get xorm path from github: $xormCmd."
    cd $goSrc
    git clone "https://$xormCmd" "$xormCmd"
    go get -v "$xormCmd/xorm"
fi

if [[ "$xormUpdateFlag" = "yes" ]]; then
    echo "==> print: updating $xormCmd/xorm."
    go get -u "$xormCmd/xorm"
fi
echo "==> print: start go to build and install xorm cmd."
cd $goSrc/$xormCmd/xorm
go build
go install


echo "==> print: start getting $schema's models from mysql by xorm."
cd $nowPath
gofiles=$schemaPath/*.go
if [[ "$deleteOldModels" == "yes" ]]; then
    echo "==> print: start delete old $schema's models except views."
    # rm -f $gofiles
    ls $gofiles | grep -v view | xargs rm -f
fi

xorm reverse mysql "$dbConnector/$schema?charset=utf8" "$goSrc/$xormCmd/xorm/templates/goxorm" "$schemaPath"
echo "++++++++++++++++++++++++ "$dbConnector/$schema?charset=utf8""
echo ">>>>>>>>>>>>"$goSrc/$xormCmd/xorm/templates/goxorm" "$schemaPath""
echo "==> print: start formatting $schema's models by own rules."

formatfiles=`ls $gofiles|grep -v view`
for i in $formatfiles; do
    echo "==> print: start formatting $i."
    include=`grep -E $amountKeys $i |grep BIGINT`
    if [[ "$include" != "" ]]; then
        importInclude=`grep import $i`
        if [[ "$importInclude" != "" ]]; then
            awk '{if ($0 ~ /\)$/) {print "\n\t\"github.com/gogap/types\"\n\)"} else {print}}' $i > $i.bak
        else
            awk '{if ($0 ~ /^package/) { print "package '"$schema"'\nimport\(\n\t\"github.com/gogap/types\"\n\)\n"} else {print}}' $i > $i.bak
        fi
        mv $i.bak $i

        awk -v keys="$amountKeys" '{if ($1 ~ keys && ($3 ~ "BIGINT" || $5 ~ "BIGINT" || $7 ~ "BIGINT")) {$2="types.Amount";print} else {print}}' $i > $i.bak
        mv $i.bak $i
    fi
    # awk -F ' ' '/TINYINT/{print "\t"$1"\tbool\t"$3}' $i > $i.bak
    awk '{if ($3~/TINYINT/ || $5~/TINYINT/ || $7~/TINYINT/){$2="bool";print} else {print}}' $i > $i.bak
    mv $i.bak $i
    # awk -F ' ' '/int /{print "\t"$1"\tint32\t"$3}{print}' $i > $i.bak
    awk '{if ($1 != "Version" && $2~/^int$/){$2="int32";print} else {print}}' $i > $i.bak
    mv $i.bak $i
    awk '{if ($1~/^Version$/){print "Version int \`xorm:\"version\"\`"} else {print}}' $i > $i.bak
    mv $i.bak $i
    # awk -F ' ' '/CreateTime/{print "CreateTime time.Time `xorm:"DATETIME created""' $i > $i.bak
    awk '{if ($1~/^CreateTime$/){print "CreateTime time.Time \`xorm:\"DATETIME created\"\`"} else {print}}' $i > $i.bak
    mv $i.bak $i
    awk '{if ($1~/^LogTime$/){print "LogTime time.Time \`xorm:\"DATETIME created\"\`"} else {print}}' $i > $i.bak
    mv $i.bak $i
    awk '{if ($1~/^UpdateTime$/){print "UpdateTime time.Time \`xorm:\"DATETIME updated\"\`"} else {print}}' $i > $i.bak
    mv $i.bak $i
    awk '{if ($1~/^TradeDate$/){$2="string";print} else {print}}' $i > $i.bak
    mv $i.bak $i
done
gofmt -w $gofiles
echo "==> print: finish down $schema's models."

echo "==> print: go install download go files."
cd $schemaPath
go build
go install
echo "==> print: sync done."
